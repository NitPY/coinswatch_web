from django.db import models
import time

class Exchanges(models.Model):
    ExName = models.CharField(max_length=10,default = 'na')

class Coins(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = 'na',primary_key=True)
    CoinName = models.CharField(max_length = 50,default = 'na')
    IsInIndia = models.BooleanField(default=False)

class Watchlist(models.Model):
    coinobj = Coins.objects.filter(IsInIndia=True)      #get all coins objects which are present in india
    coins_inindia = [coin.CoinSymbol for coin in coinobj]   #crating a list of those coins
    coinschoices = tuple(zip(coins_inindia,coins_inindia))  #to be represent in the form if ((a,a)(b,b))-required by choices
    # print(coins_inindia)

    coin = models.CharField(max_length = 10, choices=coinschoices, default='Select coin')

class CoinRates(models.Model):
    '''
    This should act as permanent storage
    '''
    CoinSymbol = models.ForeignKey(Coins, on_delete=models.CASCADE)
    ExghangeID = models.IntegerField()
    SellRateUSD = models.FloatField(default = 0)
    BuyRateUSD = models.FloatField(default = 0)
    SellRateINR = models.FloatField(default = 0)
    BuyRateINR = models.FloatField(default= 0)
    TimeStamp = models.FloatField(default = 0)

class CurrentPrice(models.Model):
    '''
    This should be used to store current data,
    data from this db then to be moved to CoinRates, which is permenent storage
    '''
    CoinSymbol = models.ForeignKey(Coins, on_delete=models.CASCADE)
    ExghangeID = models.IntegerField()
    SellRateUSD = models.FloatField(default = 0)
    BuyRateUSD = models.FloatField(default = 0)
    SellRateINR = models.FloatField(default = 0)
    BuyRateINR = models.FloatField(default= 0)
    TimeStamp = models.FloatField(default = 0)



