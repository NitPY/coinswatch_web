# Generated by Django 2.0.7 on 2018-07-29 12:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Coins',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CoinSymbol', models.CharField(default='na', max_length=10)),
                ('CoinName', models.CharField(default='na', max_length=50)),
                ('IsInIndia', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Exchanges',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ExName', models.CharField(default='na', max_length=10)),
            ],
        ),
    ]
