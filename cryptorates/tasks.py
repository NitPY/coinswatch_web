from .priceextractor import Extractprice
from django.utils.crypto import get_random_string
from .models import Coins,CoinRates,CurrentPrice
from celery import shared_task
import threading
import time


data_collection_inproggress = False

@shared_task
def get_prices():
    '''
    task will populate the coin prices to to model table
    should be called once in 10 mins
    :return: None
    '''
    # exchangedict = {1:'coindelta'
    #                 2:'coinsecure',
    #                 3:'koinex',
    #                 4:'zebpay',
    #                 5:'unocoin',
    #                 6:'buyucoin',
    #                 7:'ethxindia',
    #                 8:'pocketbits',
    #                 9:'coinome',
    #                 10:'bitbns',
    #                 11:'wazirx'
    #                 }


    global data_collection_inproggress
    data_collection_inproggress = True
    print('locking function')
    print('collecting coins data')
    price = Extractprice()
    CurrentPrice.objects.all().delete()

    # data = price.get_zebpay_prices()
    # data = data['inr']
    # save_coindata_toDB(data,4)

    data = price.get_bitbns_prices()
    data = data['inr']
    save_coindata_toDB(data,10)

    data = price.get_coinome_prices()
    data = data['inr']
    save_coindata_toDB(data,9)

    data = price.get_buyucoin_prices()
    data = data['inr']
    save_coindata_toDB(data,6)

    data = price.get_koinex_prices()
    data = data['inr']
    save_coindata_toDB(data,3)

    data = price.get_coindelta_prices()
    data = data['inr']
    save_coindata_toDB(data,1)

    updatetime = int(time.time())   #
    current_data = CurrentPrice.objects.all()
    for data in current_data:
        try:
            coinfk = Coins.objects.get(CoinSymbol=str(data.CoinSymbol.CoinSymbol))
        except Coins.DoesNotExist:
            print('coin : %s does not exist'%data.CoinSymbol)
            continue
        current_data = CoinRates(
            CoinSymbol = coinfk,
            ExghangeID = data.ExghangeID,
            SellRateINR = data.SellRateINR,
            BuyRateINR = data.BuyRateINR,
            TimeStamp = updatetime)
        current_data.save()

    # time.sleep(120)
    # get_prices()
    threading.Timer(180,get_prices).start()
    return

def save_coindata_toDB(data,exchangeid):
    for coin in data:
        sellrate = str(data[coin]['Sell'])
        buyrate = str(data[coin]['Buy'])
        if len(sellrate) == 0:
            sellrate = 0.0

        if len(buyrate) == 0:
            buyrate = 0.0
        # print(coin)
        try:
            coinfk = Coins.objects.get(CoinSymbol=coin)
        except Coins.DoesNotExist:
            # print('coin : %s does not exist'%coin)
            continue
        coinobj = CurrentPrice(
            CoinSymbol = coinfk,
            ExghangeID = exchangeid,
            SellRateINR = sellrate,
            BuyRateINR = buyrate,
        TimeStamp=time.time())
        coinobj.save()


@shared_task
def get_all_coins():
    '''
    runs once in a day
    should populate the coinslist ,check if new coins are added
    populate true if the coins are in indian market.
    :return:None
    '''

    from cryptorates.models import CurrentPrice
    price = Extractprice()
    data = price.all_cmc_coins()

    for coin in data.items():
        # print(coin)
        try:
            coinsobj = Coins.objects.get(CoinSymbol=coin[0])
            if CurrentPrice.objects.filter(CoinSymbol=coin[0]).exists():
                print('CMC coin present in india')
                #check if coin from CMC exists in currentprice, if present then mark true in Coins model
                coinsobj.IsInIndia = True   #False by default otherwise
                coinsobj.save()
        except Coins.DoesNotExist:
            print('adding %s'%coin[0])
            coinobj = Coins(CoinSymbol=coin[0], CoinName=coin[1])
            coinobj.save()


