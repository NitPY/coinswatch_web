from django.contrib import admin
from .models import Exchanges,Coins
from testserver.models import TestMachines, TestNames
# Register your models here.

admin.site.register(Exchanges)
admin.site.register(Coins)
admin.site.register(TestMachines)
admin.site.register(TestNames)

