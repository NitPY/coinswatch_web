from django.db.models.signals import post_save
import django.dispatch
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from django.contrib.auth.models import User
#
def user_created(*args,**kwargs):
    #commented of peace. no hard feelings
    return
    for i in kwargs.items():
        print(i)
    print('user created')

some_signal = django.dispatch.Signal(providing_args=('arg1','arg2'))    #this declares some_signal with arguments arg1 and arg2

#@receiver(signal_name , sender_class)  #if sender is not given, any post save signal would be caught
@receiver(post_save,sender=User)
def create_token_for_new_user(sender,instance=None,created=False,**kwargs):
    print('**'*30)
    print('token created for user')
    if created:
    #if new user instance is created
        Token.objects.create(user=instance) #create token for newly created user
