from django import forms
from .models import Coins

class WatchListForm(forms.Form):

    coin = forms.CharField(label='coin', max_length=50)
