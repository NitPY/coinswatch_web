from django.urls import path
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = [
    path('',views.home,name='home'),
    path('collect/', views.collectdata, name='coindatacollect'),
    path('collectcoin/', views.collectcoindata, name='coincollect'),
    path('watchlist/',views.watchlist, name='watchlistview'),

]

urlpatterns = format_suffix_patterns(urlpatterns)