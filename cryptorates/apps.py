from django.apps import AppConfig


class CryptoratesConfig(AppConfig):
    name = 'cryptorates'

    def ready(self):
        from .signals import some_signal,user_created
        some_signal.connect(user_created)