from django.shortcuts import redirect
from django.http import HttpResponse
# from django.
class LoginMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response


    def __call__(self, request):
        #redirecting view to login
        if not request.user.is_authenticated and not request.path == '/accounts/login/':
            print(request.path)


            print('user is authenticated')
            return redirect('loginview')
        return self.get_response(request)

