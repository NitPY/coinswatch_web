from django.shortcuts import render,render_to_response
from django.http import HttpResponse
from .tasks import get_all_coins,get_prices
from .models import CurrentPrice, CoinRates,Coins
from django.db.models import Count,Avg,Func
from django.contrib.auth.decorators import login_required
import threading
from .forms import WatchListForm
from django.template import RequestContext
from .signals import some_signal
from django.core.cache import cache


data_collection_inproggress = False
# Create your views here.
class Round(Func):
    function = 'ROUND'
    template='%(function)s(%(expressions)s, 2)'

def error_404(request, exception):
    render(request, 'cryptorates/error.html')


def watchlist(request):
    '''
    to add the watchlist of coins peruser
    :param request:
    :return:
    '''

    context = {}
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = WatchListForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            coin = form.cleaned_data['coin']
            # print('watchilist coin %s'%coin)
            response = render(request, 'cryptorates/watchlist.html',context=context)
            if 'coinslist' not in request.COOKIES:
                # setting a new cookie named coinslist if
                # a cookie with this name is not present
                response.set_cookie('coinslist',coin)

            else:
                #if cookie is already present, append the data,
                #cookie is only stored in string format
                coinlist = request.COOKIES['coinslist']
                coinlist += '_'+coin
                response.set_cookie('coinslist', coinlist)
                coinlistdisplay = coinlist.split('_')
                context['coinscookie'] = coinlistdisplay

    # if a GET (or any other method) we'll create a lank form
    else:
        form = WatchListForm()
        response = render(request, 'cryptorates/watchlist.html',context=context)
    return response
# @login_required(login_url='/accounts/login/')
def home(request):
    # get_all_coins()
    # if not request.user.is_authenticated:
    #     # return HttpResponse('<h1>Please Login</h1>')
    # else:
    #     user = request.user.username
    if not cache.get('homedata'):   #if homedata is not found in cache
        print('data not cached')
        histdict = {}   #contains past data
        currdict = {}
        currenttimestamp = int(CurrentPrice.objects.all()[0].TimeStamp)
        number_seconds_n_hour = 3600
        filtertimestampstart = currenttimestamp-number_seconds_n_hour-50
        filtertimestampend = currenttimestamp-number_seconds_n_hour+50  #180 is time interval at which data is collected in by priceextractor
        histrydata = CoinRates.objects.values('CoinSymbol').annotate(sellrate=Round(Avg('SellRateINR')),buyrate=Round(Avg('BuyRateINR'))).filter(TimeStamp__range=(filtertimestampstart,filtertimestampend))
        #first database access
        # print('histrydata count %s'%histrydata.count())
        if histrydata.count() == 0:
            #if there is no matching histrydata, use current data as histrydata.
            #idealy we should use data near to the timestamp we are expecting
            #as sometime if server is down, that data could be missed. That is the improvement
            print('Calculating histrydata from currentdata ')
            histrydata = CurrentPrice.objects.values('CoinSymbol').annotate(sellrate=Round(Avg('SellRateINR')),
                                                                         buyrate=Round(Avg('BuyRateINR')))
            print('histrydata count %s'%histrydata.count())
        for coin in histrydata:
            # print(coin)
            histdict[coin['CoinSymbol']] = [coin['sellrate'], coin['buyrate']]
        currentdata = CurrentPrice.objects.values('CoinSymbol').annotate(sellrate=Round(Avg('SellRateINR')),buyrate=Round(Avg('BuyRateINR'))).order_by('CoinSymbol')
        for coin in currentdata:
            # print(coin)
            if coin['sellrate']==0 or coin['buyrate']==0:
                continue
            currdict[coin['CoinSymbol'].lower()] = [coin['sellrate'], coin['buyrate'],\
                                                    round(100*((coin['sellrate']-histdict[coin['CoinSymbol']][0])/coin['sellrate']),2),\
                                                    round(100*((coin['buyrate']-histdict[coin['CoinSymbol']][1])/coin['buyrate']),2)]
        cache.set('homedata',currdict,180)  #set expire time as 180 seconds on homedata cache
        print('data has been cached')
    else:
        print('used data from cache')
        currdict = cache.get('homedata')    #use cached data
    context = {'curr_rate':currdict}
    some_signal.send(sender=Coins, args1='Nani', args2='Naru Hodo') #some signal implementation need to study
    return render(request,'cryptorates/homepage.html',context=context)

@login_required(login_url='/accounts/login/')
def collectcoindata(request):
    get_all_coins()

    return HttpResponse('<h1>coindate collected<\h1>')
@login_required(login_url='/accounts/login/')
def collectdata(request):

    global data_collection_inproggress
    if not data_collection_inproggress:
        data_collection_inproggress=True
        get_prices()
        return HttpResponse('<h1>price collection started</h1>')

        # return HttpResponse('<h1>price collection started<\h1>')
    else:
        return HttpResponse('<h1>price collection in proggress</h1>')
