from rest_framework import permissions

class IsUserReadOnly(permissions.BasePermission):
    '''
    permission to allow users with superuser status to read, ie get method
    return true if user can post to Database else False
    '''

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        # Write permissions are only allowed to the superuser.
        if request.user.is_superuser:
            print('user is superuser %s'%request.user.is_superuser)
            return True
        else:
            print('user is not superuser %s' % request.user.is_superuser)
            return False