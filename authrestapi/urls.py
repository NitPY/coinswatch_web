from django.urls import path, include
from . import views
from rest_framework.authtoken import views as rest_framework_views
urlpatterns = [
    path('allcoins',views.CoinResponse.as_view()),
    path('coins',views.GetQueriedCoin.as_view()),#for rest api query
    path('specific/<str:coin>/',views.GetSpecificCoin.as_view()),
    path('restauth',rest_framework_views.obtain_auth_token,name='get_auth_token',)
    # posting to this view with username and password data gives back token key
]