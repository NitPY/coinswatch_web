from cryptorates.models import Coins,CurrentPrice
from rest_framework import serializers

class CoinSerializers(serializers.ModelSerializer):

    class Meta:
        model = Coins
        fields = ('CoinSymbol','CoinName','IsInIndia')