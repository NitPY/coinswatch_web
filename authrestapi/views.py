from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from cryptorates.models import Coins,CurrentPrice
from .serializers import CoinSerializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status,permissions
from .permissions import IsUserReadOnly #custom permission
from rest_framework.views import APIView
from .customthrottl import ThrottleGroup

from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle, AnonRateThrottle
from rest_framework import generics
# Create your views here.
#refernce https://realpython.com/django-rest-framework-quick-start/

class GetQueriedCoin(generics.ListAPIView):
    # serializer_class = CoinSerializers
    #http://139.59.30.95:8002/auth/coins?coin=XLM
    def get_queryset(self): #for query normal url is needed, not '/<int : param>'
        queriedcoin = self.request.query_params.get('coin', None) #query the coin
        print('queried coin %s'%queriedcoin)
        return Coins.objects.filter(CoinSymbol=queriedcoin)


class GetSpecificCoin(generics.ListAPIView):
    # serializer_class = CoinSerializers
    #139.59.30.95:8002/auth/specific/BTC/
    def get_queryset(self):
        """
        need to specify the serializer
        return data should be queryset
        """
        coin = self.kwargs['coin']  #this the dictionary and coin is the extracted from the url
        coindata = Coins.objects.filter(CoinSymbol=coin)
        # serializedata = CoinSerializers(coindata, many=True)
        print(coin)
        return coindata
        # return Response(serializedata.data, status=status.HTTP_200_OK)
        # print('coin : %s'%coin)


class CoinResponse(APIView):
    #provide all the coins
    #http://139.59.30.95:8001/auth/allcoins
    # permission_classes = (IsUserReadOnly,)

    throttle_classes = (ThrottleGroup,) #throttling
    def get(self,request,format=None):
        coindata = Coins.objects.all()
        serializeddata = CoinSerializers(coindata, many=True)
        return Response(serializeddata.data, status=status.HTTP_200_OK)
#
# @api_view(['GET','POST'])
# def coinsresponse(request):
#     permission_classes = (IsUserReadOnly,)
#     if request.method == 'GET':
#         coindata = Coins.objects.all()
#         serializeddata = CoinSerializers(coindata, many=True)
#         return Response(serializeddata.data, status = status.HTTP_200_OK)
#     elif request.method == 'POST':
#         print('post request came')
#         data = {'CoinSymbol':request.POST['coinsymbol'],'CoinName':request.POST['coinname'],'IsInIndia':request.POST['isinindia']}
#         serializerdata = CoinSerializers(data=data)
#         if serializerdata.is_valid():
#             serializerdata.save()
#             return Response(serializerdata.data, status=status.HTTP_201_CREATED)
#         return Response(serializerdata.errors, status=status.HTTP_400_BAD_REQUEST)  #status added
#
#
    def post(self,request, *args,**kwargs):
        print('post request came')
        data = {'CoinSymbol':request.POST['coinsymbol'],'CoinName':request.POST['coinname'],'IsInIndia':request.POST['isinindia']}  #extract data from request
        serializerdata = CoinSerializers(data=data)
        if serializerdata.is_valid():

            token ='3e2f190723691d500124c005d511a22e8b5cfea2'
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            if request.user.username == 'admin' and token ==token.key:
                serializerdata.save()
                return JsonResponse(serializerdata.data, status=status.HTTP_201_CREATED)
            else:
                return JsonResponse(status=status.HTTP_400_BAD_REQUEST)
        return Response(serializerdata.errors, status=status.HTTP_400_BAD_REQUEST)  #status added

