from django.http import HttpResponse
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate
from .forms import RegistrationForm

def register(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = RegistrationForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            username = form.cleaned_data['username']    #this the name field in html form
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirm_password']
            emailid = form.cleaned_data['email']

            user = User.objects.create_user(username=username, password=password, email=emailid)
            user.save() #add user to database
            return HttpResponse('<h1>registered successfully</h1>')
        else:
            print('form is not valid')
            return render(request, 'registration/register.html',{'form':form})
    # if a GET (or any other method) we'll create a blank form
    else:
        form = RegistrationForm()
    print('register')
    return render(request, 'registration/register.html',{'form':form})