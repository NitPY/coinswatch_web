from django.apps import AppConfig
# from django.contrib.auth.models import User
from django.db.models.signals import post_save

class LoginConfig(AppConfig):
    name = 'login'
    verbose_name = ('login')

    def ready(self):
        from cryptorates.signals import user_created
        post_save.connect(user_created)

