from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
import string


def long_enough(pw):
    ' must be at least 6 characters'
    return len(pw) >= 6


def short_enough(pw):
    ' cannot be more than 12 characters'
    return len(pw) <= 12


def has_lowercase(pw):
    ' must contain a lowercase letter'
    return len(set(string.ascii_lowercase).intersection(pw)) > 0


def has_uppercase(pw):
    ' must contain an uppercase letter'
    return len(set(string.ascii_uppercase).intersection(pw)) > 0


def has_numeric(pw):
    ' must contain a digit'
    return len(set(string.digits).intersection(pw)) > 0


def has_special(pw):
    ' must contain a special character'
    return len(set(string.punctuation).intersection(pw)) > 0


# If you don't do this you cannot use Bootstrap CSS
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, #concept of django widgets has been used
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField( label="Password", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password','type':'password',}))
class RegistrationForm(forms.Form):

    username = forms.CharField(help_text='Enter a alfanumeric username',label='Username', max_length=50,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label='Password',max_length=50,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password', 'type':'password'}))
    confirm_password = forms.CharField(label='Confirm Password',max_length=50,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password', 'type':'password'}))
    email = forms.CharField(label='Email', max_length=50,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password', 'type':'password'}))

    '''
    conditions for password and username 
    minimum 6 chars
    max 12 chars
    atleast 1 numerical, 1 special, 1 caps, 1small
    '''


    def test_password(self,pw, tests=[long_enough, short_enough, has_lowercase, has_uppercase, has_numeric, has_special]):
        for test in tests:
            if not test(pw):
                raise forms.ValidationError('password%s' % test.__doc__)

        return True

    def test_username(self,uname, tests=[long_enough, short_enough]):
        #check for username already exists in db
        try:
            User.objects.get(username=uname)
        except User.DoesNotExist:
            pass
        else:
            raise forms.ValidationError('User already exists')

        #check for username conditions
        for test in tests:
            if not test(uname):
                raise forms.ValidationError('username%s' % test.__doc__)

        return True

    def clean(self):    #this gets called when is_valid() is called
        cleaned_data = super(RegistrationForm,self).clean()
        print('clean calleds')
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        username = cleaned_data.get('username')
        #validating username
        self.test_username(username)

        #validating password
        if confirm_password!=password:
            print('password validation failed')
            raise forms.ValidationError('Passwords are not identical')
        else:
            #validate for password conditions
            self.test_password(password)
        return cleaned_data