from django.db import models
from django.contrib.auth.models import User
from cryptorates.models import Coins
# Create your models here.

class UserProfile(models.Model):

    coinobj = Coins.objects.filter(IsInIndia=True)      #get all coins objects which are present in india
    coins_inindia = [coin.CoinSymbol for coin in coinobj]   #crating a list of those coins
    coinschoices = tuple(zip(coins_inindia,coins_inindia))  #to be represent in the form if ((a,a)(b,b))-required by choices
    # print(coins_inindia)
    userid = models.ForeignKey(User, on_delete = models.CASCADE)
    coin = models.CharField(max_length = 10, choices=coinschoices, default='Select coin')
    at_rate = models.IntegerField()
    quantity = models.IntegerField()
    operatiotype_choices = (    #to display whether user want ot sell, buy or just watch the coin price
        ('sell','Sell'),
        ('buy','Buy'),
        ('watch','Watch'),
    )
    operatiotype = models.CharField(max_length=10,choices=operatiotype_choices,default='buy',)
    datetime = models.DateTimeField(auto_now_add=True)