from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import UserProfile
from .forms import UserProfileForm
from django.contrib.auth.models import User
import datetime


# Create your views here.

def userprofileformview(request):

    if request.method == 'POST':
        form = UserProfileForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.userid = User.objects.get(username=request.user.username)    #get username from userid , userid is foreignkey
            model_instance.save()
            # return render(request, 'userprofile/profilepage.html')
            return redirect(userprofileview)
    else:
        form = UserProfileForm()
    return render(request, "userprofile/profileform.html", {'form': form})

def userprofileview(request):
    print('hitting profileview')
    '''some logic'''
    context = {}
    userid = User.objects.get(username=request.user.username).id
    userprofile = UserProfile.objects.filter(userid=userid)  #filter results of the current user
    context['userprofile'] = userprofile
    return render(request, 'userprofile/profilepage.html',context=context)