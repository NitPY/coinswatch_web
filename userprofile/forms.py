from django import forms
from .models import UserProfile

class UserProfileForm(forms.ModelForm):
    '''
    below is to modify the modelform to bootstrap styling
    '''
    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })
    class Meta:
        model = UserProfile
        fields = ('coin','at_rate','quantity','operatiotype',)  #fileds from model to be displayed in form