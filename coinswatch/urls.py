"""coinswatch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.contrib.auth import views as auth_views
from login.forms import LoginForm
from cryptorates.views import error_404
from django.conf.urls import handler404

urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls')),
    path('', include('cryptorates.urls')),
    path('accounts/change-password/', auth_views.PasswordChangeView.as_view()),
    path('accounts/password_change/done/',auth_views.PasswordChangeDoneView.as_view()),
    path('accounts/password_reset/',auth_views.PasswordResetView.as_view()),
    path('accounts/reset/<uidb64>/<token>/',auth_views.PasswordResetConfirmView.as_view(), name = 'password_reset_confirm'),
    path('accounts/reset/done/',auth_views.PasswordResetDoneView.as_view()),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='registration/loginpage.html', authentication_form= LoginForm),name='loginview'),
    path('accounts/logout/',auth_views.LogoutView.as_view(template_name='registration/logout.html')),
    path('profile/',include('userprofile.urls')),
    path('login/',include('login.urls')),
    path('auth/',include('authrestapi.urls')),
    path('resource/',include('testserver.urls'))
    ]

handler404 = 'cryptorates.views.error_404'