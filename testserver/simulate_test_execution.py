# importing the requests library
import requests

# defining the api-endpoint
API_ENDPOINT = "http://139.59.30.95:8001/resource/updatestatus"


from cmd import Cmd
class SimulateTest(Cmd):
    def do_start(self,data):
        data = data.split(',')
        status='start'
        launchid=data[0]
        testid=data[1]
        self.senddata(status=status,launchid=launchid, testid=testid)


    def do_stop(self,data):
        data = data.split(',')
        status='stop'
        launchid=data[0]
        testid=data[1]
        self.senddata(status=status, launchid=launchid, testid=testid)

    def senddata(self, **reqdata):
        # data to be sent to api
        print(reqdata)
        data = {'status':reqdata['status'],
                'launchid':reqdata['launchid'],
                'test':reqdata['testid']}

        # sending post request and saving response as response object
        r = requests.put(url=API_ENDPOINT, data=data)

        # extracting response text
        pastebin_url = r.status_code
        print("The URL status is:%s" % pastebin_url)



if __name__ == '__main__':
    prompt = SimulateTest()
    prompt.prompt = '> '
    prompt.cmdloop('Starting prompt...LaunchID,TestID')



