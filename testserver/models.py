from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class TestMachines(models.Model):
    MachineName = models.CharField(max_length=30, default = 'NA',unique=True)
    IsBusy = models.BooleanField(default=False)

class TestNames(models.Model):
    TestID = models.CharField(max_length=10, default='NA', unique=True)

class LaunchInfo(models.Model):
    LaunchID = models.IntegerField()
    LaunchTitle = models.CharField(max_length=200, default='No Title')
    TestMachine = models.ForeignKey(TestMachines, on_delete=models.CASCADE)
    TestID = models.ForeignKey(TestNames, on_delete=models.CASCADE)
    StartTime = models.DateTimeField(auto_now_add=True, blank=True)
    EndTime = models.DateTimeField(auto_now_add=True)
    Status = models.CharField(max_length=10,default='None')
    # User = models.ForeignKey(User, on_delete=models.CASCADE)

class UserPermession(models.Model):
    SuperUser = models.BooleanField(default=False)  #has access to change permissions of other users
    Admin = models.BooleanField(default=False)  #users have access to launch and stop tests
    default = models.BooleanField(default=True) #user can see launch info only.
