from django.urls import path
from . import views

urlpatterns = [

    path('machineform', views.add_machines),
    path('testform',views.add_tests),
    path('selectmachine',views.select_machine),
    path('selecttest', views.select_test),
    path('launch',views.launch),
    path('updatestatus',views.UpdateStatus.as_view()),
    path('dashboard',views.dashboard)

]