from django import forms
from django.forms import formset_factory
from .models import TestMachines, TestNames

class MachinesForm(forms.Form):
    MachineName = forms.CharField(max_length=50,
                  label='Machine',
                  widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Enter Book Machinename here',
            'name':'MachineName'
        })
                                  )
    def clean(self):
        try:
            TestMachines.objects.get(username=uname)
        except TestMachines.DoesNotExist:
            pass
        else:
            raise forms.ValidationError('Testmachine already exists')


MachineFormSet = formset_factory(MachinesForm,extra=3)  #extra denotes number of entries in form


class TestIDForm(forms.Form):
    TestName = forms.CharField(max_length=10,
                            label='TestID',
                               widget=forms.TextInput(attrs={
                                   'class': 'form-control',
                                   'placeholder': 'Enter Book TestID here',
                                   'name':'testname'
                               }
                               ))



TestIDFormSet = formset_factory(TestIDForm, extra=5)



class MachineSelectForm(forms.Form):
    #https://stackoverflow.com/questions/5747188/django-form-multiple-choice
    testmach = TestMachines.objects.filter(IsBusy=False)
    CHOICES = []
    for mach in testmach:
        CHOICES.append([mach.MachineName,mach.MachineName])

    picked = forms.MultipleChoiceField(choices=CHOICES, widget=forms.CheckboxSelectMultiple())

class TestSelectForm(forms.Form):
    #https://stackoverflow.com/questions/5747188/django-form-multiple-choice
    testname = TestNames.objects.all()
    CHOICES = []
    for mach in testname:
        CHOICES.append([mach.TestID,mach.TestID])

    picked = forms.MultipleChoiceField(choices=CHOICES, widget=forms.CheckboxSelectMultiple())