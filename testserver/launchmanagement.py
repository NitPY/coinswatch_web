
import queue
from .models import TestMachines, TestNames, LaunchInfo
import threading
from django.db.models import Max

class LaunchManagement:
    def __init__(self,nextlaunchID,machines=None,tests=None):
        # self.freemachines = freemachines    #queue to be initialized to number of free machines available and selected
        self.machine_queue = queue.Queue(maxsize=10)
        self.test_queue  = queue.Queue(maxsize=10)
        self.machines = machines    #should be a list of free machines
        self.ref_machines = self.create_reference_machinelist()
        self.tests = tests  #should be a list tests
        self.nextlaunchid = nextlaunchID

    def create_reference_machinelist(self):
        ref_machines = []
        for mach in self.machines:
            ref_machines.append(mach)
        return ref_machines

    def queue_tests(self):

        while not self.test_queue.full() and len(self.tests)>0:
            test = self.tests.pop()
            print(test)
            self.test_queue.put(test)

    def queue_free_machines(self):
        #queues up free machines
        testmachines = TestMachines.objects.filter(IsBusy=False)    #select machines which are not busy false
        for dbmachine in testmachines:
            if dbmachine.MachineName in self.ref_machines and not self.machine_queue.full():
                self.machine_queue.put(dbmachine.MachineName)   #enqueue freedup machine

    def launchtests(self):

        #this will be called every 180 seconds
        #if machine queue is not empty, next task will be launched
        self.queue_tests()
        self.queue_free_machines()
        launchdict = {}

        while not self.machine_queue.empty():

            machine = self.machine_queue.get()  #pop a free machine
            testmachine = TestMachines.objects.get(MachineName=machine)    #get the object corresponding to machine

            test = self.test_queue.get()    #pop queued test ID
            launchdict[machine] = test
            testidobj = TestNames.objects.get(TestID = test)

            # update launch info DB table with current launch data
            print('updating dataase')

            LaunchInfo.objects.create(LaunchID = self.nextlaunchid,
                              TestMachine = testmachine,
                              TestID =testidobj
                                      )

            testmachine.IsBusy = True  #marking the machine as busy
            testmachine.save()

        print(launchdict)   #this will go to launch CTF API
        if not self.test_queue.empty(): #if test queue is not empty call again
            threading.Timer(60,self.launchtests).start()  #
