from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import MachineFormSet, TestIDFormSet, MachineSelectForm,TestSelectForm
from .models import TestMachines, TestNames, LaunchInfo
from .launchmanagement import LaunchManagement
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Max,Count
from rest_framework import status
from datetime import datetime
# Create your views here.
class UpdateStatus(APIView):
    #implements restapi to update test result
    def put(self,request,format=None):
        print('initiated put')
        teststatus = request.POST['status']
        launchID = request.POST['launchid']
        testid = request.POST['test']
        print(teststatus,testid,launchID)
        # launchobj = LaunchInfo.objects.filter()
        try:
            launchobj = LaunchInfo.objects.filter(TestID__TestID=testid).get(LaunchID=launchID)   #get obj
        except LaunchInfo.DoesNotExist:
            print('does not exist')
            return Response(status=status.HTTP_400_BAD_REQUEST)
        print('current status %s'%launchobj.Status)
        # if launchobj.Status == 'None':
        #     print('None is true')
        print(type(teststatus))
        if teststatus=='start' and  launchobj.Status=='None':
            launchobj.Status = teststatus  # update status
            launchobj.StartTime = datetime.now()    #update time
            launchobj.save()

            return Response(status=status.HTTP_200_OK)
        elif teststatus=='stop' and launchobj.Status=='start':
            launchobj.Status = teststatus  # update status
            launchobj.EndTime = datetime.now()
            machinename = launchobj.TestMachine.MachineName
            #while stoping free the machine
            testmachineobj = TestMachines.objects.get(MachineName = machinename)
            testmachineobj.IsBusy=False
            testmachineobj.save()
            launchobj.save()

            return Response(status=status.HTTP_200_OK)
        else:
            print('error in status')
            return Response(status=status.HTTP_400_BAD_REQUEST)



def dashboard(request):
    '''
    :param request:
    :return: generate the test report dashboard
    '''

    #get data from launchinfo table
    lobj = LaunchInfo.objects.values('LaunchID', 'Status').annotate(statcount=Count('Status'))
    aggregatedata = {}
    for data in lobj:
        if data['LaunchID'] not in aggregatedata:
            aggregatedata[data['LaunchID']] = {'Pass':0,'Running':0,'Fail':0}
            if data['Status'] == 'None':
                aggregatedata[data['LaunchID']]['Fail'] = data['statcount']
            elif data['Status'] == 'start':
                aggregatedata[data['LaunchID']]['Running'] = data['statcount']
            else:
                aggregatedata[data['LaunchID']]['Pass'] = data['statcount']
        else:
            if data['Status'] == 'None':
                aggregatedata[data['LaunchID']]['Fail'] = data['statcount']
            elif data['Status'] == 'start':
                aggregatedata[data['LaunchID']]['Running'] = data['statcount']
            else:
                aggregatedata[data['LaunchID']]['Pass'] = data['statcount']
    context = {'infodata':aggregatedata}
    return render(request,'testserver/dashboard.html' , context=context)



def add_machines(request):

    if request.method == 'POST':
        formset = MachineFormSet(request.POST)
        if formset.is_valid():
            for form in formset:
                machinename = form.cleaned_data.get('MachineName')
                if machinename:
                    machine = TestMachines(MachineName = machinename)
                #
                    machine.save()
            return HttpResponse('Machines added')
    else:
        formset = MachineFormSet()
    return render(request, 'testserver/testmachineform.html', {
        'formset': formset,
        'heading': 'Add Test Machines',
    })

def add_tests(request):
    #form to add tests
    if request.method == 'POST':
        formset = TestIDFormSet(request.POST)
        if formset.is_valid():
            for form in formset:
                testname = form.cleaned_data.get('TestName')

                if testname:
                    test = TestNames(TestID=testname)
                    test.save()
            return HttpResponse('Tests added')

    else:
        formset = TestIDFormSet()
    return render(request, 'testserver/testidform.html', {
        'formset': formset,
        'heading': 'Add Tests',
    })

def select_machine(request):
    #this would be a landing page
    if request.method == 'POST':
        form = MachineSelectForm(request.POST)
        if form.is_valid():
            testmachines = form.cleaned_data.get('picked')
            print(testmachines)
            request.session['machines']=testmachines    #session used to pass the variable between different view
            return redirect(select_test)    #redirects to select tests

    else:
        form = MachineSelectForm()
    return render(request,'testserver/selectmachines.html',{'form':form})
    

def select_test(request):
    if request.method == 'POST':
        form = TestSelectForm(request.POST)
        if form.is_valid():
            testnames = form.cleaned_data.get('picked')
            print(testnames)
            request.session['testname']=testnames
            return redirect(launch) #redirect through view name
            #redirects to launch page

    else:
        form = TestSelectForm()
    return render(request,'testserver/selecttests.html',{'form':form})

def launch(request):
    # any test got launched then create the db entry
    # get the max launchId number
    maxlaunchid = LaunchInfo.objects.aggregate(maxlaunchid=Max('LaunchID'))
    if maxlaunchid['maxlaunchid'] == 'None':
        # table is empty, the return value will be None
        nextlaunchID = 1  # min launch Id should be 1
    else:
        nextlaunchID = maxlaunchid['maxlaunchid'] + 1
    launchobj = LaunchManagement(nextlaunchID,machines=request.session['machines'], tests=request.session['testname'])   #will manage queuing of machnes and tests
    print(type(request.session['machines']))
    print(request.session['machines'])
    print(request.session['testname'])
    launchobj.launchtests()
    print('current launchid  %s'%nextlaunchID)
    return HttpResponse('<h1>test launched</h1>')



