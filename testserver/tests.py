from django.test import TestCase
from .launchmanagement import LaunchManagement
from .models import TestMachines, TestNames
# Create your tests here.

class launchtest(TestCase):

    def SetUp(self):
        print('setting up data')
        machines = ['mac1','mac2','mac3','mac4']
        for mach in machines:
            TestMachines.create(MachineName=mach)
        testcases = ['test1','test2','test3','test4','test5','test6','test7','test8','test9','test10']
        for test in testcases:
            TestNames.create(TestID=test)

    def test_launchop(self):
        testmachine = TestMachines.objects.all()
        machines = []
        for mach in testmachine:
            machines.append(mach.TestMachine)

        testname = TestNames.objects.all()
        tests = []
        for test in testname:
            tests.append(test.TestID)
        launchobj = LaunchManagement(machines=machines,tests=tests)
        print(launchobj.launchtests())




