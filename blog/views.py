from django.shortcuts import render
from django.http import HttpResponse
from . import models
from . import priceextractor
from datetime import datetime, timedelta
import threading
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import ZebpaySerializer
# Create your views here.

class RestApiView(APIView):
    def get(self,request):
        price = models.zebpay.objects.all()
        # price = {'BTC':"crypto"}
        serialzer = ZebpaySerializer(price, many=True).data #
        return Response(serialzer)

def first_view(request):
    pricegetter()
    timethreshold = datetime.now() - timedelta(minutes=3)     #threshold calculation for last 3 minutes
    values = models.zebpay.objects.all().filter(DateAdded__gte = timethreshold)
    count = request.session.get('count',0)
    request.session['count'] = count+1
    context = {'values':count}
    return HttpResponse('<h1>page visited %s times<\h1>'%count)
    # return render(request,'blog/homepage.html',context=context)

def pricegetter():
    price = priceextractor.Extractprice()

    coindeltadata = price.get_buyucoin_prices()    #returns dictionary
    for coin in coindeltadata:
        p = models.coindelta(CoinSymbol=coin,BuyRate=coindeltadata[coin]['Buy'],SellRate=coindeltadata[coin]['Sell'],)
        print('coindelta')
        p.save()


    koinexdata = price.get_koinex_prices()    #returns dictionary
    for coin in koinexdata:
        p = models.koinex(CoinSymbol=coin,BuyRate=koinexdata[coin]['Buy'],SellRate=koinexdata[coin]['Sell'],)
        print('koin')
        p.save()


    coinomedata = price.get_coinome_prices()    #returns dictionary
    for coin in coinomedata:
        p = models.coinome(CoinSymbol=coin,BuyRate=coinomedata[coin]['Buy'],SellRate=coinomedata[coin]['Sell'],)
        print('coinome')
        p.save()

    buyucoindata = price.get_buyucoin_prices()    #returns dictionary
    for coin in buyucoindata:
        p = models.buyucoin(CoinSymbol=coin,BuyRate=buyucoindata[coin]['Buy'],SellRate=buyucoindata[coin]['Sell'],)
        print('buyucoin')
        p.save()

    pocketbitsdata = price.get_pocketbits_prices()  # returns dictionary
    for coin in pocketbitsdata:
        p = models.pocketbits(CoinSymbol=coin, BuyRate=pocketbitsdata[coin]['Buy'],
                             SellRate=pocketbitsdata[coin]['Sell'], )
        print('pocket')
        p.save()

    bitbnsdata = price.get_bitbns_prices()  # returns dictionary
    for coin in bitbnsdata:
        p = models.bitbns(CoinSymbol=coin, BuyRate=bitbnsdata[coin]['Buy'],
                             SellRate=bitbnsdata[coin]['Sell'], )
        print('bitbns')
        p.save()


    zebpaydata = price.get_zebpay_prices()  # returns dictionary
    for coin in zebpaydata:
        p = models.zebpay(CoinSymbol=coin, BuyRate=zebpaydata[coin]['Buy'],
                             SellRate=zebpaydata[coin]['Sell'], )
        print('zebpay')
        p.save()
    threading.Timer(180, pricegetter).start()

    allcoins = price.get_all_coins(zebpaydata,coindeltadata,buyucoindata,coinomedata,koinexdata,pocketbitsdata,bitbnsdata)
    for coin in allcoins:
        if not models.AllCoins.objects.filter(CoinSymbol=coin).exists():
            p=models.AllCoins(CoinSymbol=coin)
            # print(coin)
            p.save()
    return