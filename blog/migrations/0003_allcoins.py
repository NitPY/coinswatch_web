# Generated by Django 2.0.7 on 2018-07-22 08:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20180720_1600'),
    ]

    operations = [
        migrations.CreateModel(
            name='AllCoins',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CoinSymbol', models.CharField(default='', max_length=10)),
            ],
        ),
    ]
