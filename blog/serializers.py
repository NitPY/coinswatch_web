from rest_framework import serializers
from .models import zebpay
class ZebpaySerializer(serializers.ModelSerializer):
    class Meta:
        model = zebpay
        fields = "__all__"

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return zebpay.objects.create(**validated_data)