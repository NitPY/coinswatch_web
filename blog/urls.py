from django.urls import path
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = [
    path('',views.first_view,name='login'),
    path('zebpay/',views.RestApiView.as_view()),
    ]

urlpatterns = format_suffix_patterns(urlpatterns)