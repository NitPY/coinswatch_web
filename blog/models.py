from django.db import models
from django.utils import timezone
class Blog_With_Index(models.Model):
    title = models.CharField(db_index=True, max_length=100)
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    body = models.TextField()

class Blog_WO_Index(models.Model):
    title = models.CharField(max_length=100)
    added = models.DateTimeField(auto_now_add=True)
    body = models.TextField()


# Create your models here.
class coindelta(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = 'na')
    BuyRate = models.FloatField(default=0)
    SellRate = models.FloatField(default=0)
    DateAdded = models.DateTimeField(auto_now_add=True)

class koinex(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = 'na')
    BuyRate = models.FloatField(default=0)
    SellRate = models.FloatField(default=0)
    DateAdded = models.DateTimeField(auto_now_add=True)

class zebpay(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = 'na')
    BuyRate = models.FloatField(default=0)
    SellRate = models.FloatField(default=0)
    DateAdded = models.DateTimeField(auto_now_add=True)

class buyucoin(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = 'na')
    BuyRate = models.FloatField(default=0)
    SellRate = models.FloatField(default=0)
    DateAdded = models.DateTimeField(auto_now_add=True)
class pocketbits(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = 'na')
    BuyRate = models.FloatField(default=0)
    SellRate = models.FloatField(default=0)
    DateAdded = models.DateTimeField(auto_now_add=True)

class coinome(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = 'na')
    BuyRate = models.FloatField(default=0)
    SellRate = models.FloatField(default=0)
    DateAdded = models.DateTimeField(auto_now_add=True)

class bitbns(models.Model):
    CoinSymbol = models.CharField(max_length=10,default = '')
    BuyRate = models.FloatField(default=0)
    SellRate = models.FloatField(default=0)
    DateAdded = models.DateTimeField(auto_now_add=True)

class AllCoins(models.Model):
    CoinSymbol = models.CharField(max_length=10, default = '')





